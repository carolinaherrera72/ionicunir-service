import { Component } from '@angular/core';
import {ApiService} from "../services/api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    data: any;

  constructor(private apiService:ApiService,private router: Router) {
this.getInfo()
  }
  getInfo(){
    this.apiService.getData('bicicletas')
        .subscribe(
            (data:any) => { // Success
              this.data=data.results;
                console.error( this.data);

            },
            (error) => {
              console.error(error);
            }
        );
  }
}
