import { Component } from '@angular/core';
import {ApiService} from "../services/api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  data: any;

  constructor(private apiService:ApiService,private router: Router) {
    this.getInfo()
  }
  getInfo(){
    this.apiService.getData('tenis')
        .subscribe(
            (data:any) => { // Success
              this.data=data.results;
              console.error( this.data);

            },
            (error) => {
              console.error(error);
            }
        );
  }
}
