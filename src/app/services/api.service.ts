import { Injectable } from '@angular/core';
import {HttpClient, } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private  http: HttpClient) {

  }
  private ruta = 'https://api.mercadolibre.com/sites/MLA/search?q=';

  getData(id: string | undefined) {
    return this.http.get(this.ruta+id);
  }

}
