import { Component } from '@angular/core';
import {ApiService} from "../services/api.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  data: any;

  constructor(private apiService:ApiService,private router: Router) {
    this.getInfo()
  }
  getInfo(){
    this.apiService.getData('peluches')
        .subscribe(
            (data:any) => { // Success
              this.data=data.results;
              console.error( this.data);

            },
            (error) => {
              console.error(error);
            }
        );
  }
}
